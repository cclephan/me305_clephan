import pyb
import utime

pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
pinB5 = pyb.Pin(pyb.Pin.cpu.B5)

pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
pinB1 = pyb.Pin(pyb.Pin.cpu.B1)

class Task_Motor:
    ''' @brief                  Encoder task runninng encoder driver functions
        @details                Keeps track of encoder position and delta, while also setting calling the set position
                                function in driver to 0 when zero boolean is true.
    '''
    
    
    def __init__(self, period,duty_shares, motor_drv):

        ''' 
        @brief              Constructs an motor task object
        @details            Instantiates period, a variable changing for every period, motor object
        @param              Period at which motor updates defined by user in main.
                            MotorShare is a share object that contains all relevent info about the motor
        
        '''
        
        
        ## @brief Defines period as what is called in main for period parameter
        self.period = period
        ## @brief Time adjusts once clock reaches the period value plus the current time
        self.next_time = period + utime.ticks_ms()

        # @brief Shares all relevavant motor info
        # [Encoder number (int), Current Position (ticks), Current delta (ticks/period), duty(int), Zero (boolean), Fault(boolean)]
        self.duty_shares = duty_shares
        
        self.motor_drv = motor_drv


        ## @brief Creates encoder object
        # if self.MotorShare.read(ID) == 1:
            #  @details Creates encoder object with functionallity described in encoder driver file. Parameters for object dictate encoder pins and timer.
        self.motor1 = self.motor_drv.motor(1,pinB4,pinB5)
        # elif self.MotorShare.read(ID) == 2:
            #  @details Creates encoder object with functionallity described in encoder driver file. Parameters for object dictate encoder pins and timer.
        self.motor2 = self.motor_drv.motor(3,pinB0,pinB1)
            
        self.motor1.set_duty(0)
        self.motor2.set_duty(0)
    
    def run(self):
        ''' 
        @brief      Runs encoder task zeroing encoder and updating position when necessary
        @details    Function that is run in main setting encoder position to 0 if it receives zero command from task user.
                    Task also updates encoder position/delta and returns the tuple to be used in user task if required.
        @param      Boolean value that is sent from user task to encoder task when the user wants to zero the encoder. If true the encoder is zero'd.
        '''

        # If the current time passes next time (time to update) then next update is utilized to obtain encoder position and delta
        if (utime.ticks_ms() >= self.next_time):
                    # If zero command is True then encoder position is set to 0
            self.next_time += self.period
            
            duty = self.duty_shares.read(0)
            self.motor1.set_duty(duty) 
            
            duty = self.duty_shares.read(1)
            self.motor1.set_duty(duty) 
            
            
        
        