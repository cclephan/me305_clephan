# -*- coding: utf-8 -*-
"""
Created on Tue Nov 16 12:24:39 2021

@author: johna
"""

import pyb
import utime
from ulab import numpy as np



prompt = ['Touch Left Bottom (Origin)', 
          'Touch Left Middle', 
          'Touch Left Top', 
          'Touch Middle Top', 
          'Touch Center', 
          'Touch Middle Bottom', 
          'Touch Right Bottom', 
          'Touch Right Middle', 
          'Touch Right Top']



class TouchPanel:
    ''' @brief                  Interface with quadrature encoders
        @details                Contains all basic encoder functionallity that will be sent to task encoder when task user
                                requests encoder information.
    '''
    
    def __init__(self):

        ''' 
        @brief      Constructs an encoder object
        @details    Creates timer thats number is defined in task encoder, counting up to 16 bit overflow keeping track of encoder position. 
                    The timer channels can be defined in task encoder where pins are defined for the hardware encoders.
                    Lastly two variables are created one accounting for overflow while one can go up and down infinitely.
        @param      Pinch1 defines channel one pin in task encoder
        @param      Pinch2 defines channel two pin in task encoder
        @param      timerNum defines the timer number that will be used for pyb Timer.
        '''
        self.A0 = pyb.Pin.cpu.A0
        self.A1 = pyb.Pin.cpu.A1
        self.A6 = pyb.Pin.cpu.A6
        self.A7 = pyb.Pin.cpu.A7
        self.out = pyb.Pin.OUT_PP
        self.inn = pyb.Pin.IN
        
        self.pin = pyb.Pin
        self.ADC = pyb.ADC
        
        self.pin(self.A0,self.out,value = 1)
        self.pin(self.A7,self.out,value = 0)
        self.ADC(self.A1)
        self.ADC(self.A6)
        
        # self.getTime = utime.ticks_us
        # self.tdif = utime.ticks_diff
        self.wait = utime.sleep_us
        
        # (self.xcur,self.ycur,self.zcur) = (0,0,False)
        
        (self.Kxx, self.Kxy, self.Kyx, self.Kyy, self.xc, self.yc) = (1,0,0,1,0,0)

        # self.Vxcur = 0
        # self.Vycur = 0
        # self.t0 = self.getTime()

    def setCalV(self,calV):
        (self.Kxx, self.Kxy, self.Kyx, self.Kyy, self.xc, self.yc) = calV              

    
    @micropython.native    
    def getScan(self):
        '''@brief Scans X,Y,Z of touchpad
        
        '''
        self.pin(self.A0,self.out,value = 1)
        self.wait(4)
        ADCxm = self.ADC(self.A1)
        z = ADCxm.read() > 69
        self.wait(4)
        self.pin(self.A6,self.out,value = 0)
        self.wait(4)
        ADCxp = self.ADC(self.A7)        
        yr = ADCxp.read()
        self.wait(4)
        self.pin(self.A1,self.out,value = 1)
        self.pin(self.A7,self.out,value = 0)
        self.wait(4)
        ADCym = self.ADC(self.A0)
        self.pin(self.A6,self.inn)
        xr = ADCym.read() 
        x = xr*self.Kxx+yr*self.Kxy+self.xc
        y = xr*self.Kyx+yr*self.Kyy+self.yc
        return (x,y,z)
    
# if __name__ == '__main__':
#     obj = TouchPanel()
#     while (True):
#         #Attempt to run FSM unless Ctrl+c is hit
        
#         try:
#             print(obj.getPosVel())
#             utime.sleep_ms(50)
#             print('\n')
#             to = utime.ticks_us()
#             obj.contactPoint()
#             obj.getPosVel()
#             # tdif = utime.ticks_diff(utime.ticks_us(),to)
#             # print(tdif)
            
#         except KeyboardInterrupt:
#             break
#     to = utime.ticks_us()
#     obj.getScan()
#     tdif = utime.ticks_diff(utime.ticks_us(),to)
#     print(tdif)
    


    