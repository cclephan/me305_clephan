import utime
import tp
from ulab import numpy as np
import os

prompt = ['Touch Left Bottom (Origin)', 
          'Touch Left Middle', 
          'Touch Left Top', 
          'Touch Middle Top', 
          'Touch Center', 
          'Touch Middle Bottom', 
          'Touch Right Bottom', 
          'Touch Right Middle', 
          'Touch Right Top']

class Task_TP:
    
    
    def __init__(self,period,Share):
        self.tp = tp.TouchPanel()
        
        self.getTime = utime.ticks_us
        self.tdif = utime.ticks_diff
        self.wait = utime.sleep_us
        
        ## @brief Defines period as what is called in main for period parameter
        self.period = period
        ## @brief Time adjusts once clock reaches the period value plus the current time
        self.next_time = period + self.getTime()
        
        (self.xcur,self.ycur,self.zcur,self.Vxcur,self.Vycur,self.t0) = (0,0,False,0,0,0)
        
        calV = self.getCalCoef()
        self.tp.setCalV(calV)
        
        self.Share = Share
        
    def getCalCoef(self):
        filename = "RT_cal_coeffs.txt"
        if filename in os.listdir():
            with open(filename, 'r') as f:
                # Read the first line of the file
                cal_string = f.readline()
                # Split the line into multiple strings
                # and then convert each one to a float
                cal_values = tuple([float(cal_value) for cal_value in cal_string.strip().split(',')])
        else:
            with open(filename, 'w') as f:
                # Perform manual calibration
                cal_values = self.calibrate()
                (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = cal_values
                # Then, write the calibration coefficients to the file
                # as a string. The example uses an f-string, but you can
                # use string.format() if you prefer
                f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
                
        return cal_values    

    def calibrate(self):
        # yscale = 100/4095
        # xscale = 176/4095
        # yc = 176/2 - 35
        # xc = 50 + 35
        X = np.ones((9,3))
        ADC_vals = np.ones((9,2))
        X_act = np.array([[-88,-50],[-88,0],[-88, 50],[0,50],[0,0],[0,-50],[88,-50],[88,0],[88,50]])
        n = 0
        r = False
        #and ([25,25] > abs(X_act[1,:] + [x*xscale - xc,y*yscale - yc])).all()
        while(n<9):
            (x,y,z) = self.tp.getScan()
            if z and r :
                ADC_vals[n,:] = [x,y]
                r = False
                print('REMOVE HAND NOW!!!!')
                print(ADC_vals) 
                n += 1
            elif z == False and r == False:
                r = True
                print(prompt[n])
                self.wait(200000)
                print('go now')                
                
        X[:,0:2] = ADC_vals
        Y = np.dot(X.transpose(),X_act)
        print(X)
        print(Y)
        B = np.dot(np.linalg.inv(np.dot(X.transpose(),X)),Y)
        print(B)
        
        return (B[0,0], B[0,1], B[1,0], B[1,1], B[2,0], B[2,1])
        
    
    
    def contactPoint(self):
        (x,y,z) = self.tp.getScan()
        if not self.zcur and z:
            self.xcur = x
            self.Vxcur = 0
            self.ycur = y
            self.Vycur = 0
            self.zcur = z
            self.t0 = self.getTime()
        elif z:
            alpha = 0.85
            beta = 0.005
            T_s = self.tdif(self.getTime(),self.t0)/1E6
            self.t0 = self.getTime()
            xcur_temp = self.xcur
            ycur_temp = self.ycur
            self.xcur = xcur_temp+alpha*(x-xcur_temp)+T_s*self.Vxcur
            self.ycur = self.ycur+(y-ycur_temp)+T_s*self.Vycur
            self.Vxcur = self.Vxcur+beta/T_s*(x-xcur_temp)
            self.Vycur = self.Vycur+beta/T_s*(y-ycur_temp)
            #print(T_s)
        else:
            self.zcur  = False
            self.xcur  = 0
            self.Vxcur = 0
            self.ycur  = 0
            self.Vycur = 0
            
    def getPosVel(self):
        self.contactPoint()
        self.Share.write(0,(self.xcur,self.ycur,self.Vxcur,self.Vycur))
        
    
    def run(self):
        
        if (utime.ticks_ms() >= self.next_time):
            # If zero command is True then encoder position is set to 0
            self.next_time += self.period
                        
            self.getPosVel()
    