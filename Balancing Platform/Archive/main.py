from pyb import I2C
import IMU
import tp
import utime
from ulab import numpy as np
import motor
import task_motor
import shares

##  @brief IMU task period (1.5 milliseconds)
T_encoder = 1.5
##  @brief Moter task period (2 milliseconds)
T_motor = 2
##  @brief User task period (40 milliseconds)
T_user = 40

## Motor Shares Index referances
ID = 0
DUTY = 1

## @brief Creates motor driver object
motor_drv = motor.DRV8847(3)

Motor1Share = shares.ShareMotorInfo([1,0])
Motor2Share = shares.ShareMotorInfo([2,0])

##  @brief Creating a variable for the motor task in the Task_Motor Class at period T_motor
motorTask1 = task_motor.Task_Motor(T_motor,Motor1Share,motor_drv)
motorTask2 = task_motor.Task_Motor(T_motor,Motor2Share,motor_drv)
if __name__ == '__main__':
    tp = tp.TouchPanel()
    print('ranning')
    i2c = I2C(1,I2C.MASTER)
    i2c.init(I2C.MASTER, baudrate=400000)
    IMU_driver = IMU.BNO055(0x28, i2c)
    
    # Controller Data
    K = np.array([[-.09,-.6,-8*5,-.01]])
    #K = np.array([[-.8822,-.441,-.336,-.0266]])
    R = 2.21 # oms
    Kt = 13.8
    Vdc = 12
    
    C = 1000*R/(4*Kt*Vdc)
    
    deg2rad = 3.14159/180
    
    
    
    while (True):
        try:
            #update Ball Pos
            tp.contactPoint()
            
            # Build State
            (x,xdot,y,ydot) = tp.getPosVel()     
            (h, th_x, th_y) = IMU_driver.readEuler()
            (hdot, thd_x, thd_y)  = IMU_driver.readOmega()
            th_x *= deg2rad
            th_y *= deg2rad
            thd_x *= deg2rad
            thd_y *= deg2rad
            #print((hdot, thd_y, thd_x))
            # State 1 
            q1 = np.array([[x],[xdot],[th_y],[thd_y]])
            q2 = np.array([[y],[ydot],[th_x],[thd_x]])
            #print(q1)
            # Torque
            T1 = np.dot(-K,q1)
            T2 = -np.dot(-K,q2)
            print(T2)
            # Duty
            D1 = C*T1[0,0]
            D2 = C*T2[0,0]
            Motor1Share.write(DUTY,D1)
            Motor2Share.write(DUTY,D2)
            print(D2)
            print('\n\n\n')
            # Motor Commands
            motorTask1.run()    
            motorTask2.run()
            
        except KeyboardInterrupt:      
            Motor1Share.write(DUTY,0)
            Motor2Share.write(DUTY,0)
            break
    
    Motor1Share.write(DUTY,0)
    Motor2Share.write(DUTY,0)
    print('Program Terminating')
    

