from ulab import numpy as np
import utime

ID = 0
DUTY = 1
STATE = 2
EULER = 3
OMEGA = 4

# Controller Data
K = np.array([[-.09,-.6,-8*5,-.01]])
#K = np.array([[-.8822,-.441,-.336,-.0266]])
R = 2.21 # oms
Kt = 13.8
Vdc = 12

C = 1000*R/(4*Kt*Vdc)

deg2rad = 3.14159/180
class Task_Controller:
    
    def __init__(self, period, Share):
        ## @brief Defines period as what is called in main for period parameter
        self.period = period
        ## @brief Time adjusts once clock reaches the period value plus the current time
        self.next_time = period + utime.ticks_ms()

        self.Share = Share
        
    def control(self):
        if self.Share.read(ID) == 1:
            # Build State
            (x,xdot) = self.Share.read(STATE)
            (h, th_x, th_y) = self.Share.read(EULER)
            (hdot, thd_x, thd_y) = self.Share.read(OMEGA)
            th_x *= deg2rad
            th_y *= deg2rad
            thd_x *= deg2rad
            thd_y *= deg2rad
            #print((hdot, thd_y, thd_x))
            # State 1 
            q = np.array([[x],[xdot],[th_y],[thd_y]])
            #print(q1)
            # Torque
            T = -np.dot(-K,q)
            print(T)
            # Duty
            D = C*T[0,0]
            self.Share.write(DUTY,D)
            print(D)
            print('\n\n\n')
        else:
            (y,ydot) = self.Share.read(STATE)
            (h, th_x, th_y) = self.Share.read(EULER)
            (hdot, thd_x, thd_y)  = self.Share.read(OMEGA)
            th_x *= deg2rad
            th_y *= deg2rad
            thd_x *= deg2rad
            thd_y *= deg2rad
            
            q = np.array([[y],[ydot],[th_x],[thd_x]])
            T = -np.dot(-K,q)
            print(T)
            D = C*T[0,0]
            self.Share.write(DUTY,D)
            
    def run(self):
        