from pyb import I2C
import IMU
import tp
import motor
import task_motor
import shares
import task_control
import task_TP

##  @brief IMU task period (1.5 milliseconds)
T_tp = 1.5
##  @brief Moter task period (2 milliseconds)
T_motor = 2
##  @brief User task period (40 milliseconds)
T_user = 40

T_control = 2

## Motor Shares Index referances
ID = 0
DUTY = 1
STATE = 2
EULER = 3
OMEGA = 4
## @brief Creates motor driver object
motor_drv = motor.DRV8847(3)

ball_share = shares.ShareMotorInfo([(0,0,0,0)])
IMU_share = shares.ShareMotorInfo([(0,0,0,0)])
duty_share = shares.ShareMotorInfo([0,0])



##  @brief Creating a variable for the motor task in the Task_Motor Class at period T_motor
motorTask = task_motor.Task_Motor(T_motor,duty_share,motor_drv)

tpTask = task_TP.Task_TP(T_tp,ball_share)

cntrlTask = task_control.Task_Controller(T_control)

if __name__ == '__main__':
    tp = tp.TouchPanel()
    print('ranning')
    i2c = I2C(1,I2C.MASTER)
    i2c.init(I2C.MASTER, baudrate=400000)
    IMU_driver = IMU.BNO055(0x28, i2c)
    
    
    
    while (True):
        try:
            tpTask.run()
            task_control.run()
            # Motor Commands
            motorTask.run()    
            
        except KeyboardInterrupt:      
            Motor1Share.write(DUTY,0)
            Motor2Share.write(DUTY,0)
            break
    
    Motor1Share.write(DUTY,0)
    Motor2Share.write(DUTY,0)
    print('Program Terminating')
    

