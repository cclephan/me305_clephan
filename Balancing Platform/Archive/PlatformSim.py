import numpy as np
import matplotlib.pyplot as plt

# Simulation
def simP(K,q,tstop):
    ''' 
        @brief              Runs the Ball Plate Simulation
        @details            Uses a given set up for the ball plate system and achieves balance
        @param              K Controller for torque. 4 by 1 matrix
        @param              q initial conditions 1 by 4 matrix [Ball Position, Platform Theta, Ball Velocity, Platform Velocity]
        @param              tstop Stop condition
        @return             simt time data from simulation m by matrix
        @return             sim state variables at each time step m by 4 matrix [Ball Position, Platform Theta, Ball Velocity, Platform Velocity]        
    '''  
    t = 0
    sim = np.array(q)
    simt = np.array([[t]])
    while t < tstop:
        qdot = (B@-K + A)@q
        q = q + qdot*dt
        sim = np.append(sim,q,axis=1)    
        simt = np.append(simt,[[t]],axis=1)  
        t += dt
    return (simt,sim)   

#Plot
def plot(simt,sim,n):
    ''' 
        @brief              Plots the dynamic responce of the Ball Plate Simulation
        @details            Plot 1: Ball Position
                            Plot 2: Platform Theta
                            Plot 3: Ball Velocity
                            Plot 4: Platform Velocity
        @param              simt time data from simulation m by matrix
        @param              sim state variables at each time step m by 4 matrix [Ball Position, Platform Theta, Ball Velocity, Platform Velocity]
        @param              n Simulation Number
        ''' 
    x_o = sim[0,0]
    
    plt.figure(n<<2)
    plt.plot(simt[0,:],sim[0,:])
    plt.ylabel('Ball Position, X [mm]');
    plt.xlabel('Time (s)');
    plt.title('Sim {:}: Open-Loop x_o = {:}mm, X'.format(n,x_o));
    
    plt.figure((n<<2) +1)
    plt.plot(simt[0,:],sim[1,:]);
    plt.ylabel('Platform Angle, Thetay [rad]');
    plt.xlabel('Time (s)');
    plt.title('Sim {:}: Open-Loop x_o = {:}mm, Thetay'.format(n,x_o));
    
    plt.figure((n<<2) +2)
    plt.plot(simt[0,:],sim[2,:]);
    plt.ylabel('Ball Velocity, Xdot [mm/s]');
    plt.xlabel('Time (s)');
    plt.title('Sim {:}: Open-Loop x_o = {:}mm, Xdot'.format(n,x_o));
    
    plt.figure((n<<2) +3)
    plt.plot(simt[0,:],sim[3,:]);
    plt.ylabel('Angular Velocity, Thetadoty [rad/s]');
    plt.xlabel('Time (s)');
    plt.title('Sim {:}: Open-Loop x_o = {:}mm, Thetadot_y'.format(n,x_o));
   
# Constants
plt.clf()
## @brief A matrix is a State jacobian matrix recorded from Hw0x02
A = np.array([[0,0,1,0],[0,0,0,1],[-5.2170,4.0111E3,0,177.2680],[.1129,64.8295,0,-3.8358]])
## @brief B matrix is a Torque jacobian matrix recorded from Hw0x02
B = np.array([[0],[0],[3.2499E-5],[-7.0323E-7]])
## @brief dt is the time step between each state in the simulation
dt = .0001
    
# Sim1 Variables Coditions
tstop = 1
K = np.array([[0 , 0 , 0 , 0]])
q = np.array([[0],[0],[0],[0]])
(simt,sim) = simP(K,q,tstop)
plot(simt,sim,1)

# Sim2 Variables Coditions
tstop = .4
K = np.array([[0 , 0 , 0 , 0]])
q = np.array([[50],[0],[0],[0]])
(simt,sim) = simP(K,q,tstop)
plot(simt,sim,2)

dt = .001
# Sim3 Variables Coditions
tstop = 25
K = np.array([[-300000, -200000000, -50000, -20000000]])
q = np.array([[50],[0],[0],[0]])
(simt,sim) = simP(K,q,tstop)
plot(simt,sim,3)